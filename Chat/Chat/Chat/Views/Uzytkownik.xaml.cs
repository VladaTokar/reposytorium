﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Chat.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();

			zd.Source = ImageSource.FromUri(new Uri("https://i.pinimg.com/564x/fc/f6/51/fcf6518d4a1dc492165cae11d8088eb6.jpg"));
		}

		async void Btn_wyloguj(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new Logowanie());
		}
	}
}