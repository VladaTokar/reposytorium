﻿using Chat.Tables;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Chat.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Logowanie : ContentPage
	{
		public Logowanie()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();
		}

		async void btn_rejestracja(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new Rejestracja());
		}

		async void btn_zaloguj(object sender, EventArgs e)
		{
			var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DataBase.db");
			var db = new SQLiteConnection(path);
			var myquery = db.Table<RejestracjaUzytkownika>().Where(u => u.Imie.Equals(Imie.Text) && u.Haslo.Equals(Haslo.Text)).FirstOrDefault();

			if (myquery.Imie.Equals("Vlada") && myquery.Haslo.Equals("vlada"))
			{
				App.Current.MainPage = new NavigationPage(new StronaDomowa());
			}
			else if (myquery.Imie.Equals("Pies") && myquery.Haslo.Equals("pies"))
			{
				App.Current.MainPage = new NavigationPage(new Page1());
			}
			else
		
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					var result = await this.DisplayAlert("Error", "Failed User Name and Password", "Yes", "Cancel");
					if (result)
						await Navigation.PushAsync(new Logowanie());
					else
					{
						await Navigation.PushAsync(new Logowanie ());
					}
				});
			}


		}
	}
	
}